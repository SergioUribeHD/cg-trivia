﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ResponseDisplay : MonoBehaviour
{
    [SerializeField] private bool showSuccesfulMessages = false;
    [SerializeField] private GameObject popup = null;
    [SerializeField] private Text messageText = null;

    [Header("Error messages")]
    [SerializeField] private string networkErrorMsg = "Error al cargar los datos. Por favor inténtalo más tarde.";
    [SerializeField] private List<ErrCode> errCodes = new List<ErrCode>();

    private void Awake()
    {
        RestApiRequest.OnResponse += HandleResponse;
        RestApiRequest.OnRequestSent += DisablePopup;
    }

    private void OnDestroy()
    {
        RestApiRequest.OnResponse -= HandleResponse;
        RestApiRequest.OnRequestSent -= DisablePopup;
    }


    private void HandleResponse(UnityWebRequest response)
    {
        if (response.result == UnityWebRequest.Result.ConnectionError || response.result == UnityWebRequest.Result.ProtocolError)
            EnablePopup(errCodes.TryFind(e => e.Code == response.responseCode, out var errCode) ? errCode.ErrorMsg : networkErrorMsg);
        else if (!showSuccesfulMessages)
            DisablePopup();
    }

    private void EnablePopup(string message)
    {
        popup.SetActive(true);
        messageText.text = message;
    }

    private void DisablePopup()
    {
        popup.SetActive(false);
    }
}

[System.Serializable]
public class ErrCode
{
    [field: SerializeField] public int Code { get; private set; }
    [field: SerializeField] public string ErrorMsg { get; private set; }
}
