﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AvatarDisplay : MonoBehaviour
{
    [SerializeField] private Image bkgImage = null;
    [SerializeField] private RawImage avatarRawImage = null;

    private void Start()
    {
        avatarRawImage.texture = Texture2D.blackTexture;
        bkgImage.enabled = false;
        User.OnAvatarSet += UpdateAvatar;
        UpdateAvatar();
    }

    private void OnDestroy()
    {
        User.OnAvatarSet -= UpdateAvatar;
    }

    private void UpdateAvatar()
    {
        bkgImage.enabled = true;
        avatarRawImage.texture = User.Avatar == null ? Texture2D.blackTexture : User.Avatar;
    }
}
