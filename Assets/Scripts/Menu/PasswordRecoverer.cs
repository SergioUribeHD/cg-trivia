﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PasswordRecoverer : MonoBehaviour
{
    [SerializeField] private InputField emailInput = null;
    [SerializeField] private InputField loginEmailInput = null;
    [SerializeField] private string recoverPassUrl = "http://ec2-34-205-31-162.compute-1.amazonaws.com:8080/uac-api-test/auth/reset";
    [SerializeField] private UnityEvent onSuccess = null;

    private void OnEnable()
    {
        if (string.IsNullOrWhiteSpace(loginEmailInput.text)) return;
        emailInput.text = loginEmailInput.text;
    }

    public void RecoverPassword()
    {
        RestApiObject restObj = new RestApiObject
        {
            uri = recoverPassUrl,
            bodyJson = new EmailRecovery { email = emailInput.text },
            onSuccess = HandleEmailRetrievedSuccessfully
        };
        Debug.Log(((EmailRecovery)restObj.bodyJson).email);
        Debug.Log(restObj.uri);
        RestApiRequest.Instance.PostJson(restObj);
    }

    private void HandleEmailRetrievedSuccessfully(DownloadHandler response)
    {
        onSuccess?.Invoke();
    }
}

[System.Serializable]
public class EmailRecovery
{
    public string email;
}
