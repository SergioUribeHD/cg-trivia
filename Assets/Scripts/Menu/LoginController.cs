﻿using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.UI;

public class LoginController : MonoBehaviour
{
    [SerializeField] private InputField emailInput = null;
    [SerializeField] private InputField passwordInput = null;
    [SerializeField] private Button loginButton = null;
    [SerializeField] private string urlLoginApi = "http://ec2-18-206-205-69.compute-1.amazonaws.com:8080/uac/auth/login";
    [SerializeField] private string urlTeamsApi = "http://ec2-18-206-205-69.compute-1.amazonaws.com:8080/uac/workteams/search/user/{userId}/{orgId}/";
    [SerializeField] private string userIdWildcard = "{userId}";
    [SerializeField] private string orgIdWildcard = "{orgId}";
    [SerializeField] private string urlEnrollmentGroupsApi = "http://ec2-18-206-205-69.compute-1.amazonaws.com:8080/cbm-ro-games/getEnrollmentGroupByUserGame/";
    [SerializeField] private string urlCategoriesApi = "http://ec2-18-206-205-69.compute-1.amazonaws.com:8080/games/getTriviaCategoryByListTrivia/";
    [SerializeField] private UnityEvent onLoginSucceeded = null;

    private TeamsAndAttributesUser userTeamsAndAttrsBody;

    private void OnEnable()
    {
        ValidateButton();
    }

    public void TryLogin()
    {
        loginButton.interactable = false;
#if UNITY_EDITOR
        if (emailInput.text.ToLower().Equals("test") && passwordInput.text.ToLower().Equals("test"))
        {
            onLoginSucceeded?.Invoke();
            return;
        }
#endif
        var loginData = new LoginData { email = emailInput.text, password = passwordInput.text };
        //print($"Email: {loginData.email}. Password: {loginData.password}");
        RestApiRequest.Instance.PostJson(urlLoginApi, loginData, HandleAuthSucceeded, null);
    }


    public void ValidateButton() => loginButton.interactable = !string.IsNullOrEmpty(emailInput.text) && !string.IsNullOrEmpty(passwordInput.text);

    private void HandleAuthSucceeded(DownloadHandler response)
    {
        var userData = JsonUtility.FromJson<UserData>(response.text);
        var attrsUser = JsonUtility.FromJson<AttributesUser>(response.text);
        userTeamsAndAttrsBody = new TeamsAndAttributesUser
        {
            user_id = userData.userid,
            org_id = userData.orgId,
            listAttr = Attr.ToArray(attrsUser.ToArray())
        };
        User.LogIn(userData);
        //onLoginSucceeded?.Invoke();
        RestApiRequest.Instance.TryGetTexture(userData.orgMobileLogoUrl, HandleAvatarDownloadSucceeded, null);
    }

    private void HandleAuthFailed(long errorCode)
    {
        // TODO Feedback to user
    }

    private void HandleAvatarDownloadSucceeded(Texture2D tex)
    {
        User.Avatar = tex;
        string teamsUrl = urlTeamsApi.Replace(userIdWildcard, User.Id.ToString());
        teamsUrl = teamsUrl.Replace(orgIdWildcard, User.OrgId.ToString());
        RestApiRequest.Instance.GetJson(teamsUrl, User.Token, HandleWorkTeamsRequestSucceded, null);
    }

    private void HandleWorkTeamsRequestSucceded(DownloadHandler response)
    {
        userTeamsAndAttrsBody.listTeam = Team.ToArray(response.text);
        RestApiRequest.Instance.PostJson(urlEnrollmentGroupsApi, userTeamsAndAttrsBody, User.Token, HandleEnrollmentGroupsRequestSucceeded, null);

    }

    private void HandleEnrollmentGroupsRequestSucceeded(DownloadHandler response)
    {
        foreach (var eg in JSON.Parse(response.text))
        {
            SessionData.EnrollmentGroupsData.Add(eg.Value);
        }
        var enrollmentGroupsUser = new EnrollmentGroupsUser(SessionData.EnrollmentGroupsData);
        RestApiRequest.Instance.PostJson(urlCategoriesApi, enrollmentGroupsUser, User.Token, HandleCategoryRequestSucceeded, null);
    }

    private void HandleCategoryRequestSucceeded(DownloadHandler response)
    {
        //var userDataUrl = $"http://ec2-34-205-31-162.compute-1.amazonaws.com:8080/uac-api-test/users/{User.Id}";
        foreach (var cat in JSON.Parse(response.text))
        {
            SessionData.CategoriesData.Add(cat.Value);
        }
        onLoginSucceeded?.Invoke();
    }

}

[System.Serializable]
public class LoginData
{
    public string email;
    public string password;
}

