﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Switcher : MonoBehaviour
{
    [SerializeField] private Slider slider = null;
    [SerializeField] private Toggle toggle = null;
    [SerializeField] private float handleMoveSpeed = 7f;
    [field: SerializeField] public UnityEvent<bool> OnSwitch { get; private set; }

    public bool IsOn
    {
        get => toggle.isOn;
        set => toggle.isOn = value;
    }

    private void Awake()
    {
        toggle.onValueChanged.AddListener(HandleSwitch);
    }

    private void HandleSwitch(bool on)
    {
        OnSwitch?.Invoke(on);
        StartCoroutine(MoveHandle(on));
    }

    private IEnumerator MoveHandle(bool on)
    {
        slider.value = on ? slider.minValue : slider.maxValue;
        float aimValue = on ? slider.maxValue : slider.minValue;
        toggle.interactable = false;
        while (Mathf.Abs(slider.value - aimValue) > Mathf.Epsilon)
        {
            yield return null;
            float increaseRate = (on ? 1 : -1) * handleMoveSpeed * Time.deltaTime;
            slider.value += increaseRate;
        }
        toggle.interactable = true;

    }
}
