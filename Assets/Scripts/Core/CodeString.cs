﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class CodesText
{
    [SerializeField] private Text text = null;
    [SerializeField] private string[] codes = new string[1] { string.Empty };

    private string originalText;

    public void Update(object[] objs)
    {
        if (string.IsNullOrEmpty(originalText))
            originalText = text.text;
        string replacedText = originalText;
        for (int i = 0; i < codes.Length; i++)
            replacedText = replacedText.Replace(codes[i], objs[i].ToString());
        text.text = replacedText;
    }
}


[System.Serializable]
public class CodeString
{
    [SerializeField] private string code = string.Empty;
    [SerializeField] private string value = string.Empty;

    public string Code => code;
    public string Value => value;
}
