﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RankingType
{
    NotDefined,
    /// <summary>
    /// Ranking of users total points in the game
    /// </summary>
    User,
    /// <summary>
    /// Ranking of users in a certain category
    /// </summary>
    UserCategory,
    /// <summary>
    /// Ranking of users in a certain enrollment group
    /// </summary>
    EnrollmentGroup,
    /// <summary>
    /// Ranking of enrollment groups in a certain category
    /// </summary>
    EnrollmentGroupsCategory
}

public enum RankingSubType
{
    NotDefined,
    Points,
    Accumulated,
    MaxStage,
    Attempts
}