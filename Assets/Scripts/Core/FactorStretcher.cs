﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FactorStretcher : MonoBehaviour
{
    [SerializeField] private bool stretchWidth = false;
    [SerializeField] private bool stretchHeight = true;
    [SerializeField] private bool useScale = false;

    public static CanvasScaler Canvas
    {
        get
        {
            if (canvas == null)
                canvas = FindObjectOfType<CanvasScaler>();
            return canvas;
        }
    }

    private static CanvasScaler canvas = null;

    private RectTransform rectTransform;
    private Vector2 initialRectDimensions;
    private Vector2 rectStandardResolution;
    private Vector2 initialAnchoredPos;

    private void Start()
    {
        rectTransform = (RectTransform)transform;
        float refResWidth = Canvas.referenceResolution.x - rectTransform.offsetMin.x + rectTransform.offsetMax.x;
        float refResHeight = Canvas.referenceResolution.y - rectTransform.offsetMin.y + rectTransform.offsetMax.y;
        rectStandardResolution = new Vector2(refResWidth, refResHeight);
        initialRectDimensions = new Vector2(rectTransform.rect.width, rectTransform.rect.height);
        initialAnchoredPos = rectTransform.anchoredPosition;
    }

    private void Update()
    {
        if (stretchHeight)
        {
            float factor = 1 /( useScale ? transform.localScale.x : rectTransform.rect.width / rectStandardResolution.x);
            rectTransform.sizeDelta = new Vector2(useScale ? initialRectDimensions.x : rectTransform.sizeDelta.x, initialRectDimensions.y * factor);
            //rectTransform.anchoredPosition = new Vector2(initialAnchoredPos.x, initialAnchoredPos.y * transform.localScale.x);
        }
        else if (stretchWidth)
        {
            float factor = 1 /( useScale ? transform.localScale.y : rectTransform.rect.height / rectStandardResolution.y);
            rectTransform.sizeDelta = new Vector2(initialRectDimensions.x * factor, initialRectDimensions.y);
            rectTransform.anchoredPosition = new Vector2(initialAnchoredPos.x, initialAnchoredPos.y * transform.localScale.x);
        }

    }
}

