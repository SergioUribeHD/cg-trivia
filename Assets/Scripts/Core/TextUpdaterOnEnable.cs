﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextUpdaterOnEnable : MonoBehaviour
{
    [SerializeField] private Text text = null;

    [TextArea(3, 5)] 
    [SerializeField] private string updatingText = string.Empty;

    private void OnEnable()
    {
        text.text = updatingText;
    }
}
