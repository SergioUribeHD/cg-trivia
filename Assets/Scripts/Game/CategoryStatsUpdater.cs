﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CategoryStatsUpdater : MonoBehaviour
{
    [SerializeField] private string postUrl = "http://ec2-34-205-31-162.compute-1.amazonaws.com:8080/cgd-api-test/game/trivia/reportTriviaGameData/";

    private void Awake()
    {
        ScoreHandler.OnStageAttemptAdded  += UpdateStats;
    }


    private void OnDestroy()
    {
        ScoreHandler.OnStageAttemptAdded -= UpdateStats;
    }

    private void UpdateStats()
    {
        if (SessionData.CurrentCategory == null) return;
        var restObj = new RestApiObject
        {
            uri = postUrl,
            bodyJson = new CategoryStats(SessionData.CurrentCategory),
            token = User.Token,
            onSuccess = HandleStatsUpdateSucceeded
        };

        print(JsonUtility.ToJson(restObj.bodyJson));

        RestApiRequest.Instance.PostJson(restObj);
    }

    private void HandleStatsUpdateSucceeded(DownloadHandler response)
    {

    }
}

[System.Serializable]
public class CategoryStats
{
    public long userId;
    public long catId;
    public long egId;
    public int points;
    public bool completed;

    /// <summary>
    /// Completed stages count
    /// </summary>
    public int stagesOK;

    /// <summary>
    /// Category's stage with max points
    /// </summary>
    public int maxStage;

    /// <summary>
    /// Accumulated category points that add up no matter the user wins or loses.
    /// </summary>
    public int totPoints;

    /// <summary>
    /// Total attempts of all category stages
    /// </summary>
    public int totAttempts;

    public StageStats[] listStages;

    public CategoryStats(Category category)
    {
        userId = User.Id;
        catId = category.Data.id;
        egId = category.EnrollmentGroupId;
        listStages = new StageStats[category.Stages.Count];
        completed = category.CompletedStagesCount == category.Data.stages;
        stagesOK = category.CompletedStagesCount;
        totPoints = category.AccumulatedScore;
        maxStage = category.HighestStageScore;
        for (int i = 0; i < listStages.Length; i++)
        {
            listStages[i] = new StageStats(category.Stages[i]);
            points += category.Stages[i].Score;
            totAttempts += category.Stages[i].Attempts;
        }
        category.Score = points;
        category.TotalAttempts = totAttempts;
    }
}

[System.Serializable]
public class StageStats
{
    public int points;
    public int attempts;
    public bool completed;
    public QuestionStats[] listQuestion;

    public StageStats(StageInfo stage)
    {
        points = stage.Score;
        attempts = stage.Attempts;
        listQuestion = new QuestionStats[stage.Questions.Count];
        for (int i = 0; i < listQuestion.Length; i++)
            listQuestion[i] = new QuestionStats(stage.Questions[i]);
        completed = stage.Completed;
    }
}

[System.Serializable]
public class QuestionStats
{
    public long id;

    public QuestionStats(QuestionData question)
    {
        id = question.id;
    }
}