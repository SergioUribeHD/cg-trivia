﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LivesDisplay : MonoBehaviour
{
    [SerializeField] LivesController livesController = null;
    [SerializeField] private Text livesText = null;

    private void Awake()
    {
        livesController.OnLivesUpdated += HandleLivesUpdated;
    }

    private void OnDestroy()
    {
        livesController.OnLivesUpdated -= HandleLivesUpdated;
    }

    private void HandleLivesUpdated(int oldLives, int newLives)
    {
        livesText.text = newLives.ToString();
    }
}
