﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreDisplay : MonoBehaviour
{
    [SerializeField] private Text scoreText = null;
    [SerializeField] private VarReplacer varReplacer = null;

    private void Awake()
    {
        ScoreHandler.OnScoreUpdated += HandleScoreUpdated;
    }

    private void OnDestroy()
    {
        ScoreHandler.OnScoreUpdated -= HandleScoreUpdated;
    }

    private void HandleScoreUpdated(int newScore)
    {
        scoreText.text = varReplacer.GetReplacedText(newScore);
    }
}
