﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageHandler : MonoBehaviour
{
    [SerializeField] private float dwellUntilNextQuestion = 1f;
    [SerializeField] private List<QuestionData> questions = new List<QuestionData>();
    [SerializeField] private List<DwellEvent> onCorrectOption = new List<DwellEvent>();
    [SerializeField] private List<DwellEvent> onWrongOption = new List<DwellEvent>();
    [SerializeField] private List<DwellEvent> onQuestionUpdate = new List<DwellEvent>();
    [SerializeField] private List<DwellEvent> onQuestionReady = new List<DwellEvent>();

    public static event Action<QuestionData> OnQuestionUpdated = null;
    public static event Action<int, int> OnProgressUpdated = null;
    public static event Action<StageSummary> OnStageCompleted = null;

    public bool Stop { get; set; }

    private List<QuestionData> usedQuestions = new List<QuestionData>();
    private int currentQuestionProgress;
    private QuestionData CurrentQuestion => usedQuestions[usedQuestions.Count - 1];
    private StageSummary stageSummary;

    private void Awake()
    {
        OptionContainer.OnOptionSelected += HandleOptionSelected;
        MultipleChoiceHandler.OnOptionsSubmitted += HandleOptionsSubmitted;
    }

    private void Start()
    {
        SetQuestions();
        SetStageSummary();
        UpdateQuestion();
    }

    private void SetStageSummary()
    {
        bool enoughQuestions = SessionData.CurrentCategory != null && SessionData.CurrentCategory.Data.questionsByStage <= questions.Count;
        int questionsPerStage = enoughQuestions ? SessionData.CurrentCategory.Data.questionsByStage : questions.Count;
        stageSummary = new StageSummary(questionsPerStage);

    }

    private void SetQuestions()
    {
        if (SessionData.CurrentCategory == null) return;
        questions = new List<QuestionData>(SessionData.CurrentCategory.CurrentStageQuestions);
    }

    private void OnDestroy()
    {   
        OptionContainer.OnOptionSelected -= HandleOptionSelected;
        MultipleChoiceHandler.OnOptionsSubmitted -= HandleOptionsSubmitted;
    }

    private void HandleOptionSelected(OptionContainer option)
    {
        stageSummary.AddQuestionSummary(new QuestionSummary(CurrentQuestion, option));
        if (option.IsCorrect)
            HandleRightOption();
        else
            HandleWrongOption();
    }

    private void HandleOptionsSubmitted(List<OptionContainer> selectedOptions)
    {
        bool isCorrect = selectedOptions.Count == selectedOptions.FindAll(o => o.IsCorrect).Count;
        stageSummary.AddQuestionSummary(new QuestionSummary(CurrentQuestion, selectedOptions, isCorrect));
        if (isCorrect)
            HandleRightOption();
        else
            HandleWrongOption();
    }

    public void AddCurrentQuestionToSummary(bool isCorrect) => stageSummary.AddQuestionSummary(new QuestionSummary(CurrentQuestion, isCorrect));

    private void HandleRightOption()
    {
        SessionData.CurrentCategory?.AddCorrectQuestion(CurrentQuestion);
        StartCoroutine(CallQuestionUpdate(onCorrectOption));
    }

    public void HandleWrongOption()
    {
        StartCoroutine(CallQuestionUpdate(onWrongOption));
    }

    private void UpdateQuestion()
    {
        if (currentQuestionProgress == stageSummary.QuestionsCount || questions.Count == 0)
        {
            SessionData.CurrentCategory?.MarkCurrentStageComplete(usedQuestions);
            OnStageCompleted?.Invoke(stageSummary);
            return;
        }
        currentQuestionProgress++;
        usedQuestions.Add(questions.GetRandomItem(true));
        OnQuestionUpdated?.Invoke(CurrentQuestion);
        Debug.Log($"{CurrentQuestion.id}\n{CurrentQuestion.GetCorrectOptions()[0].text}\nMultiresponse: {CurrentQuestion.multiresponse}");
        OnProgressUpdated?.Invoke(currentQuestionProgress, stageSummary.QuestionsCount);
        StartCoroutine(CallDwellEvents(onQuestionReady));
    }

    private IEnumerator CallDwellEvents(List<DwellEvent> dwellEvents)
    {
        foreach(var dwellEvent in dwellEvents)
        {
            dwellEvent.actionEvent?.Invoke();
            yield return new WaitForSeconds(dwellEvent.delay);
        }
    }    

    private IEnumerator CallQuestionUpdate(List<DwellEvent> dwellEvents)
    {
        bool noDwellInEvents = dwellEvents.FindAll(a => a.delay == 0).Count == dwellEvents.Count;
        if (dwellEvents.Count == 0 || noDwellInEvents)
            yield return new WaitForSeconds(dwellUntilNextQuestion);
        yield return CallDwellEvents(dwellEvents);
        yield return CallDwellEvents(onQuestionUpdate);
        if (Stop) yield break;
        UpdateQuestion();
    }

}

public class StageSummary
{
    public static event Action<StageSummary> OnSummaryDataUpdated = null;
    public int QuestionsCount { get; private set; }
    public int CorrectQuestionsCount => questionSummaries.FindAll(q => q.ChoseCorrectOption).Count;

    public StageSummary(int initialQuestionsCount)
    {
        QuestionsCount = initialQuestionsCount;
    }

    private List<QuestionSummary> questionSummaries = new List<QuestionSummary>();

    public IList<QuestionSummary> QuestionSummaries => questionSummaries.AsReadOnly();

    public void AddQuestionSummary(QuestionSummary question)
    {
        questionSummaries.Add(question);
        OnSummaryDataUpdated?.Invoke(this);
    }
}

public class QuestionSummary
{
    public string QuestionTitle { get; private set; }
    public Option CorrectOption { get; private set; }
    public Option[] SelectedOptions { get; private set; }
    public bool ChoseCorrectOption { get; private set; }
    public string Feedback { get; private set; }

    public QuestionSummary(QuestionData currentQuestion, OptionContainer option)
    {
        QuestionTitle = currentQuestion.text;
        CorrectOption = currentQuestion.GetCorrectOptions()[0];
        ChoseCorrectOption = option.IsCorrect;
        Feedback = option.Feedback == null ? CorrectOption.feedback.text : option.Feedback;
    }

    public QuestionSummary(QuestionData currentQuestion, bool isCorrect)
    {
        QuestionTitle = currentQuestion.text;
        CorrectOption = currentQuestion.GetCorrectOptions()[0];
        Feedback = CorrectOption.feedback.text;
        ChoseCorrectOption = isCorrect;
    }

    public QuestionSummary(QuestionData currentQuestion, List<OptionContainer> selectedOptions, bool isCorrect)
    {
        QuestionTitle = currentQuestion.text;
        SelectedOptions = selectedOptions.ToOptions();
        Feedback = GetFeedback(SelectedOptions);
        ChoseCorrectOption = isCorrect;
    }

    private static string GetFeedback(Option[] options)
    {
        string feedback = $"- {options[0].feedback.text}";
        for (int i = 1; i < options.Length; i++)
            feedback += $"\n- {options[i].feedback.text}";
        return feedback;
    }
}


