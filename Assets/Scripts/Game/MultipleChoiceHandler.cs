﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultipleChoiceHandler : MonoBehaviour
{
    [SerializeField] private GameObject blockPanel = null;

    public static event Action<List<OptionContainer>> OnOptionsSubmitted = null;

    private int correctOptionsAmount;
    private List<OptionContainer> selectedOptions = new List<OptionContainer>();

    private void Awake()
    {
        OptionContainer.OnMultipleChoiceOptionSelected += HandleMultipleChoiceSelected;
        OptionContainer.OnMultipleChoiceOptionDeselected += HandleMultipleChoiceDeselected;
        StageHandler.OnQuestionUpdated += Set;
    }

    private void OnDestroy()
    {
        OptionContainer.OnMultipleChoiceOptionSelected -= HandleMultipleChoiceSelected;
        OptionContainer.OnMultipleChoiceOptionDeselected -= HandleMultipleChoiceDeselected;
        StageHandler.OnQuestionUpdated += Set;
    }

    private void Set(QuestionData questionData)
    {
        correctOptionsAmount = questionData.GetCorrectOptions().Length;
    }    

    private void HandleMultipleChoiceSelected(OptionContainer option)
    {
        selectedOptions.Add(option);
        if (selectedOptions.Count < correctOptionsAmount) return;
        blockPanel.SetActive(true);
        OnOptionsSubmitted?.Invoke(selectedOptions);
    }   
    
    private void HandleMultipleChoiceDeselected(OptionContainer option)
    {
        selectedOptions.Remove(option);
    }    
}
