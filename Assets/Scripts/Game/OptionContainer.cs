﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionContainer : MonoBehaviour
{
    [SerializeField] private Text optionText = null;
    [SerializeField] private Button optionBtn = null;
    [SerializeField] private Image blockImg = null;
    [SerializeField] private Color selectedColor = Color.yellow;

    public static event Action<OptionContainer> OnOptionSelected = null;
    public static event Action<OptionContainer> OnMultipleChoiceOptionSelected = null;
    public static event Action<OptionContainer> OnMultipleChoiceOptionDeselected = null;

    private bool multipleChoice;

    private bool isSelected;
    public bool IsSelected
    {
        get => isSelected;
        private set
        {
            isSelected = value;
            SetBlockColor(isSelected ? selectedColor : Color.white);
            if (isSelected)
                OnMultipleChoiceOptionSelected?.Invoke(this);
            else
                OnMultipleChoiceOptionDeselected?.Invoke(this);
        }
    }

    public bool IsCorrect { get; private set; }
    public string Feedback { get; private set; }

    private void Awake()
    {
        optionBtn.onClick.AddListener(HandleOptionSelected);
    }

    private void OnDestroy()
    {
        optionBtn.onClick.RemoveListener(HandleOptionSelected);
    }

    public void SetAnswer(Option option, bool multipleChoice)
    {
        if (!gameObject.activeSelf)
            gameObject.SetActive(true);
        optionText.text = option.text;
        IsCorrect = option.isRightAnswer;
        Feedback = option.feedback.text;
        this.multipleChoice = multipleChoice;
        IsSelected = false;
    }

    private void OnDisable()
    {
        optionText.text = string.Empty;
        Feedback = null;
        IsCorrect = IsSelected = false;
    }

    private void HandleOptionSelected()
    {
        if (multipleChoice)
            IsSelected = !IsSelected;
        else
            OnOptionSelected?.Invoke(this);
    }

    public void SetBlockColor(Color color) => blockImg.color = color;

    public void SetAvailability(bool enabled)
    {
        optionBtn.interactable = enabled;
        if (!enabled)
            optionText.text = string.Empty;
    }

    public Option ToOption() => new Option { feedback = new Feedback { text = Feedback }, isRightAnswer = IsCorrect, text = optionText.text };

}

public static class ListOptionContainer
{
    public static Option[] ToOptions(this List<OptionContainer> optionContainers)
    {
        var options = new Option[optionContainers.Count];
        for (int i = 0; i < options.Length; i++)
            options[i] = optionContainers[i].ToOption();

        return options;
    }

}

