﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Timer : MonoBehaviour
{
    [SerializeField] public UnityEvent onTimeUp = null;

    public event Action<int> OnTimeUpdated = null;

    private bool paused;
    private bool stopped = true;

    private int currentTime;

    private float oneSecondTimer;

    public int StartingTime { get; set; }

    public int CurrentTime
    {
        get => currentTime;
        private set
        {
            if (value < 0) return;
            currentTime = value;
            OnTimeUpdated?.Invoke(currentTime);
            if (currentTime == 0)
                onTimeUp?.Invoke();
        }
    }

    public void StartTimer(int time)
    {
        stopped = paused = false;
        CurrentTime = StartingTime = time;
        oneSecondTimer = 0;
    }

    public void RestartTimer() => StartTimer(StartingTime);

    public void Stop() => stopped = true;

    public void PauseTimer(bool paused) => this.paused = paused;

    private void Update()
    {
        if (paused || stopped) return;
        if (oneSecondTimer < 1f)
            oneSecondTimer += Time.deltaTime;
        else
        {
            oneSecondTimer = 0f;
            CurrentTime--;
        }
    }
}
