﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressStageDisplay : MonoBehaviour
{
    [SerializeField] private Text progressText = null;
    [SerializeField] private VarReplacer progressVarReplacer = null;

    private void Awake()
    {
        StageHandler.OnProgressUpdated += HandleQuestionUpdated;
    }

    private void OnDestroy()
    {
        StageHandler.OnProgressUpdated -= HandleQuestionUpdated;
    }

    private void HandleQuestionUpdated(int currentQuestion, int questionsCount)
    {
       progressText.text = progressVarReplacer.GetReplacedText(new object[] { currentQuestion, questionsCount } );
    }
}
