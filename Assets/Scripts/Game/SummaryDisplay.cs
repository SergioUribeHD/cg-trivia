﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SummaryDisplay : MonoBehaviour
{
    [SerializeField] private SummaryPanel winSummaryPanel = null;
    [SerializeField] private SummaryPanel loseSummaryPanel = null;

    private void Awake()
    {
        StageHandler.OnStageCompleted += winSummaryPanel.Set;
        LivesStageHandler.OnAllLivesLost += loseSummaryPanel.Set;
        ScoreHandler.OnScoreUpdated += winSummaryPanel.UpdateTotalScore;
        ScoreHandler.OnScoreUpdated += loseSummaryPanel.UpdateTotalScore;
    }

    private void OnDestroy()
    {
        StageHandler.OnStageCompleted -= winSummaryPanel.Set;
        LivesStageHandler.OnAllLivesLost -= loseSummaryPanel.Set;
        ScoreHandler.OnScoreUpdated -= winSummaryPanel.UpdateTotalScore;
        ScoreHandler.OnScoreUpdated -= loseSummaryPanel.UpdateTotalScore;
    }

}
