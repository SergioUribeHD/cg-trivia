﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreHandler : MonoBehaviour
{
    [SerializeField] private int scorePerQuestion = 50;
    [SerializeField] private float maxBonusByTime = 25f;

    public static event Action<int> OnScoreUpdated = null;
    public static event Action OnStageAttemptAdded = null;

    private int timeBonus;

    public int Score
    {
        get => score;
        private set
        {
            score = value;
            OnScoreUpdated?.Invoke(value);
        }
    }

    private int score;

    private void Awake()
    {
        TimerStageHandler.OnrightOptionSelected += CalculateTimeBonus;
        StageHandler.OnQuestionUpdated += HandleQuestionUpdated;
        StageHandler.OnStageCompleted += HandleStageFinished;
        LivesStageHandler.OnAllLivesLost += HandleStageFinished;
    }

    private void Start()
    {
        Score = 0;
    }

    private void OnDestroy()
    {
        TimerStageHandler.OnrightOptionSelected -= CalculateTimeBonus;
        StageHandler.OnQuestionUpdated -= HandleQuestionUpdated;
        StageHandler.OnStageCompleted -= HandleStageFinished;
        LivesStageHandler.OnAllLivesLost -= HandleStageFinished;
    }

    public void AddScore()
    {
        Score += scorePerQuestion + timeBonus;
    }

    private void HandleQuestionUpdated(QuestionData question)
    {
        timeBonus = 0;
    }

    private void CalculateTimeBonus(int startingTime, int remainingTime)
    {
        float bonusFactor = (float)remainingTime / startingTime;
        timeBonus = (int)(maxBonusByTime * bonusFactor);
        //print($"Having a remaining time of {remainingTime}, the bonus is {timeBonus}");
    }

    private void HandleStageFinished(StageSummary summary)
    {
        SessionData.CurrentCategory?.AddAttempt(Score);
        OnStageAttemptAdded?.Invoke();
    }

}
