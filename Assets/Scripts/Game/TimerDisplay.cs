﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerDisplay : MonoBehaviour
{
    [SerializeField] private Text timeText = null;
    [SerializeField] private Timer timer = null;

    private void Awake()
    {
        timer.OnTimeUpdated += HandleTimeUpdated;
    }

    private void OnDestroy()
    {
        timer.OnTimeUpdated -= HandleTimeUpdated;
    }

    private void HandleTimeUpdated(int time)
    {
        int minutes = time / 60;
        int seconds = time % 60;
        timeText.text = $"{minutes:00}:{seconds:00}";
    }

}
