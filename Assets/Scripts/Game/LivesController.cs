﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LivesController : MonoBehaviour
{
    [SerializeField] private UnityEvent onAllLivesLost = null;

    private int lives;

    public event Action<int, int> OnLivesUpdated = null;

    public int Lives
    {
        get => lives;
        set
        {
            int newValue = Mathf.Max(0, value);
            HandleLivesUpdated(lives, newValue);
            lives = newValue;
        }
    }
    
    private void HandleLivesUpdated(int oldValue, int newValue)
    {
        OnLivesUpdated?.Invoke(oldValue, newValue);
        if (newValue == 0)
            onAllLivesLost?.Invoke();
    }
}
