﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SummaryStageBoard : MonoBehaviour
{
    [SerializeField] private SummaryQuestionLine questionSummaryPrefab = null;
    [SerializeField] private Transform content = null;

    public void Set(StageSummary stageSummary)
    {
        foreach (var questionSummary in stageSummary.QuestionSummaries)
        {
            var summaryInstance = Instantiate(questionSummaryPrefab, content);
            summaryInstance.Set(questionSummary);
        }
    }
}
