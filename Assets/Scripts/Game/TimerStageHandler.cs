﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerStageHandler : MonoBehaviour
{
    [SerializeField] private Timer timer = null;
    [SerializeField] private int initialTime = 30;

    public static event Action<int, int> OnrightOptionSelected = null;

    private void Awake()
    {
        timer.StartingTime = SessionData.CurrentCategory == null ? initialTime : SessionData.CurrentCategory.Data.GetTimeValue();
        StageHandler.OnQuestionUpdated += HandleQuestionUpdated;
        StageHandler.OnStageCompleted += HandleAllQuestionsFinished;
        OptionContainer.OnOptionSelected += HandleOptionSelected;
    }

    private void OnDestroy()
    {
        StageHandler.OnQuestionUpdated -= HandleQuestionUpdated;
        StageHandler.OnStageCompleted -= HandleAllQuestionsFinished;
        OptionContainer.OnOptionSelected -= HandleOptionSelected;
    }

    private void HandleOptionSelected(OptionContainer option)
    {
        timer.Stop();
        if (option.IsCorrect)
            OnrightOptionSelected?.Invoke(timer.StartingTime, timer.CurrentTime);
    }

    private void HandleQuestionUpdated(QuestionData question)
    {
        timer.RestartTimer();
    }

    private void HandleAllQuestionsFinished(StageSummary stage)
    {
        timer.Stop();
    }
}
