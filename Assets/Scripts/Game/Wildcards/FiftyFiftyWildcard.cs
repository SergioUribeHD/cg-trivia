﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FiftyFiftyWildcard : Wildcard
{
    [SerializeField] private QuestionContainer question = null;
    [SerializeField] private DwellEvent onHalfWrongAnswersReady = null;
    [SerializeField] private UnityEvent onHalfWrongAnswersShown = null;

    private void Awake()
    {
        StageHandler.OnQuestionUpdated += HandleQuestionUpdated;
    }

    private void OnDestroy()
    {
        StageHandler.OnQuestionUpdated -= HandleQuestionUpdated;
    }

    public void ShowWrongAnswers()
    {
        if (used) return;
        StartCoroutine(CallWrongAnswers());
    }

    private IEnumerator CallWrongAnswers()
    {
        used = true;
        onHalfWrongAnswersReady.actionEvent?.Invoke();
        yield return new WaitForSeconds(onHalfWrongAnswersReady.delay);
       foreach(var option in GetHalfWrongOptions())
            option.SetAvailability(false);
        onHalfWrongAnswersShown?.Invoke();
    }

    private List<OptionContainer> GetHalfWrongOptions()
    {
        int halfOptionsAmount = Mathf.FloorToInt(question.OptionContainersAmount / 2);
        var wrongOptions = new List<OptionContainer>();
        print(halfOptionsAmount);
        for (int i = 0; i < halfOptionsAmount; i++)
        {
            var randomWrongOption = question.GetRandomWrongOption();
            if (halfOptionsAmount > 1)
            {
                while (wrongOptions.Find(o => o.Equals(randomWrongOption)))
                    randomWrongOption = question.GetRandomWrongOption();
            }
            wrongOptions.Add(randomWrongOption);
        }
        return wrongOptions;
    }

    private void HandleQuestionUpdated(QuestionData question)
    {
        button.interactable = !used && !question.multiresponse && this.question.OptionContainersAmount > 2;
    }
}
