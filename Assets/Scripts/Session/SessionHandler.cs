﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SessionHandler : MonoBehaviour
{
    public void LogOut() => User.LogOut();
}
