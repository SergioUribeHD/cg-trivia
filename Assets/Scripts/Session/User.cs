﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public static class User
{
    public static long Id { get; private set; }
    public static long OrgId { get; private set; }
    public static string Token { get; private set; }
    public static string FirstName { get; private set; }
    public static DateTime ExpirationDate { get; private set; }
    public static Texture2D Avatar
    {
        get => avatar;
        set
        {
            avatar = value;
            OnAvatarSet?.Invoke();
        }
    }

    public static event Action OnAvatarSet = null;
    
    private static Texture2D avatar;

    public static void LogIn(UserData data)
    {
        Id = data.userid;
        OrgId = data.orgId;
        Token = data.token;
        ExpirationDate = DateTime.Parse(data.expirationDate).ToLocalTime();
        FirstName = data.firstName;
    }

    public static void LogOut()
    {
        Id = OrgId = 0;
        Token = FirstName = string.Empty;
        ExpirationDate = DateTime.MinValue;
        avatar = null;
        SessionData.Clear();
    }
}

[Serializable]
public class UserData
{
    public long userid;
    public long orgId;
    public string token;
    public string expirationDate;
    public string orgMobileLogoUrl; // TODO change to avatar variable when testing account's is not null
    public string firstName;
}

[Serializable]
public class TeamsAndAttributesUser
{
    public long user_id;
    public long org_id;
    public Team[] listTeam;
    public Attr[] listAttr;

    //public TeamsAndAttributesUser(int userId, int orgId, IEnumerable<Team> teams, IEnumerable<Attr> attrs)
    //{
    //    user_id = userId;
    //    orgId =
    //}
}

[Serializable]
public class Team
{
    public long id;
    public long workteam;

    public static Team[] ToArray(string responseText)
    {
        var teams = JSON.Parse(responseText);
        var workTeams = new Team[teams.Count];
        for (int i = 0; i < workTeams.Length; i++)
            workTeams[i] = new Team { workteam = teams[i]["id"] };
        return workTeams;
    }
}

[Serializable]
public class Attr
{
    public long custom_attribute_id;
    public long custom_attribute_type;

    public Attr(long id, string attrStr)
    {
        custom_attribute_id = id;
        custom_attribute_type = long.TryParse(attrStr, out long result) ? result : 0;
    }

    public static Attr[] ToArray(string[] attrStrs)
    {
        var attrArray = new Attr[attrStrs.Length];
        for (int i = 0; i < attrArray.Length; i++)
            attrArray[i] = new Attr(i + 1, attrStrs[i]);
        return attrArray;
    }
}

[Serializable]
public class AttributesUser
{
    public string attr1;
    public string attr2;
    public string attr3;
    public string attr4;

    public string[] ToArray() => new string[4] { attr1, attr2, attr3, attr4 };
}

[Serializable]
public class EnrollmentGroupsUser
{
    public long[] listIds;

    public EnrollmentGroupsUser(List<JSONNode> data)
    {
        var availableEnrollmentGroups = new List<EnrollmentGroup>();
        foreach (var enrollmentGroupBody in data)
        {
            int tsOpen = enrollmentGroupBody["open_date"];
            int tsClose = enrollmentGroupBody["close_date"];
            var enrollmentGroup = new EnrollmentGroup
            {
                id = enrollmentGroupBody["learning_object_id"],
                openDate = tsOpen.ToDateTime(),
                closeDate = tsClose.ToDateTime()
            };
            //Debug.Log($"Id: {enrollmentGroup.id}\nOpening date:{enrollmentGroup.openDate}\nClosing date: {enrollmentGroup.closeDate}\nIs Enrolled: {enrollmentGroup.IsWithinDate}");
            //if (enrollmentGroup.IsWithinDate)
                availableEnrollmentGroups.Add(enrollmentGroup);
        }
        listIds = new long[availableEnrollmentGroups.Count];
        for (int i = 0; i < listIds.Length; i++)
            listIds[i] = availableEnrollmentGroups[i].id;
    }

}



[Serializable]
public class EnrollmentGroup
{
    public long id;
    public DateTime openDate;
    public DateTime closeDate;

    public bool IsWithinDate => DateTime.Now > openDate && DateTime.Now < closeDate;
}
