using SimpleJSON;
using System.Collections.Generic;

[System.Serializable]
public class QuestionIdList
{
    public long[] listIds;

    public QuestionIdList(int length)
    {
        listIds = new long[length];
    }

    public QuestionIdList(int moduleIndex, JSONNode questionsData)
    {
        var ids = new List<long>();
        foreach (var question in questionsData)
        {
            if (question.Value["module"] == moduleIndex + 1)
                ids.Add(question.Value["questionId"]);
        }
        listIds = ids.ToArray();
    }
}
