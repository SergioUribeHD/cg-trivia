﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UsernameIdList
{
    public long[] userList;
    public long organizationId;

    public UsernameIdList(int listCount)
    {
        userList = new long[listCount];
    }
}
