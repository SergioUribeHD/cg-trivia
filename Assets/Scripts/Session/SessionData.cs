using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SessionData
{
    public static List<JSONNode> CategoriesData { get; } = new List<JSONNode>();
    public static List<JSONNode> EnrollmentGroupsData { get; } = new List<JSONNode>();
    public static List<Category> Categories { get; } = new List<Category>();
    public static Category CurrentCategory { get; set; }

    public static long GetEGId(long triviaId)
    {
        foreach (var item in EnrollmentGroupsData)
            if (item["learning_object_id"] == triviaId) return item["id"];
        return -1;
    }

    public static string GetEGName(long egId)
    {
        foreach (var item in EnrollmentGroupsData)
        {
            if (item["id"] == egId) return item["name"];
        }
        return null;
    }

    public static string GetEGNameWithCatTriviaId(long triviaId)
    {
        long egId = GetEGId(triviaId);
        return GetEGName(egId);
    }

    public static void Clear()
    {
        EnrollmentGroupsData.Clear();
        CategoriesData.Clear();
        Categories.Clear();
    }

    public static JSONNode GetCatQuestionModules(JSONNode dataNode) => dataNode["listTriviaCategoryQuestion"];

}
