﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressDisplay : MonoBehaviour
{
    [SerializeField] private Text progressText = null;
    [SerializeField] private VarReplacer progressVarReplacer = null;
    [SerializeField] private Dropdown categoryDdwn = null;
    [SerializeField] private ProgressBoard progressBoard = null;

    private void Awake()
    {
        categoryDdwn.onValueChanged.AddListener(HandleCategorySelected);
        CategoryLoader.OnLoadFinished += HandleCategoryLoadFinished;
    }

    private void OnEnable()
    {
        SetCategoryDropdownOptions();
        HandleCategorySelected(0);
        if (SessionData.CurrentCategory == null) return;
    }

    private void OnDestroy()
    {
        CategoryLoader.OnLoadFinished -= HandleCategoryLoadFinished;
    }

    private void SetCategoryDropdownOptions()
    {
        categoryDdwn.ClearOptions();
        var catNames = new List<string>();
        foreach (var cat in SessionData.CategoriesData)
            catNames.Add(cat["name"].Value);
        categoryDdwn.AddOptions(catNames);
    }

    private void HandleCategorySelected(int index)
    {
        var dataNode = SessionData.CategoriesData[index];
        var data = JsonUtility.FromJson<CategoryData>(dataNode);
        var questionModules = SessionData.GetCatQuestionModules(dataNode);
        CategoryLoader.Instance.Load(data, questionModules);
    }

    private void HandleCategoryLoadFinished(Category category)
    {
        var catProgressValues = new int[]
        {
            category.Score,
            category.CompletedStagesCount,
            category.Data.stages
        };
        progressText.text = progressVarReplacer.GetReplacedText(catProgressValues);
        progressBoard.Set(category);

    }
}
