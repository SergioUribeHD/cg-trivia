﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBlock : MonoBehaviour
{
    [SerializeField] private Text stageNameText = null;
    [SerializeField] private Text infoText = null;
    [SerializeField] private VarReplacer infoVarReplacer = null;
    [SerializeField] private GameObject completeMark = null;

    public void Set(string stageName, StageInfo stage)
    {
        gameObject.SetActive(true);
        stageNameText.text = stageName;
        infoText.text = infoVarReplacer.GetReplacedText(new int[] { stage.Score, stage.Attempts });
        completeMark.SetActive(stage.Completed);
    } 

    private void OnDisable()
    {
        stageNameText.text = string.Empty;
        infoText.text = string.Empty;
    }
}
