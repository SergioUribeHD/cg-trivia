﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBoard : MonoBehaviour
{
    [SerializeField] private List<ProgressBlock> progressBlocks = new List<ProgressBlock>();
    [SerializeField] private RectTransform content = null;
    [SerializeField] private string stageName = "Etapa";

    public void Set(Category category)
    {
        if (category == null) return;
        int diff = category.Stages.Count - progressBlocks.Count;
        if (diff > 0)
        {
            Debug.LogWarning($"Instantiating more blocks in {name} due to larger value than standard capacity.");
            for (int i = 0; i < diff; i++)
                Instantiate(progressBlocks[0], content);
        }
        int categoryStagesAmount = category.Data.stages;
        for (int i = 0; i < categoryStagesAmount; i++)
        {
            var stage = i < category.Stages.Count ? category.Stages[i] : new StageInfo();
            progressBlocks[i].Set($"{stageName} {i + 1}", stage);
        }
        for (int i = categoryStagesAmount; i < progressBlocks.Count; i++)
            progressBlocks[i].gameObject.SetActive(false);
    }
}
