﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MinigameContainer : MonoBehaviour
{
    [SerializeField] private MinigameContainerUI uiAttributes = new MinigameContainerUI();

    private void OnValidate()
    {
        if (uiAttributes == null) return;
        uiAttributes.UpdateColor();
        uiAttributes.UpdateImage();
        uiAttributes.UpdateTitle();
    }

}

[System.Serializable]
public class MinigameContainerUI
{
    [Header("Color")]
    [SerializeField] private Color containerColor = Color.blue;
    [SerializeField] private Image blockImg = null;
    [SerializeField] public Text playText = null;
    [Header("Image")]
    [SerializeField] private Image minigameImg = null;
    [SerializeField] private Sprite minigameSpr = null;
    [Header("Title")]
    [SerializeField] private Text titleText = null;
    [SerializeField] private string title = string.Empty;

    public void UpdateColor()
    {
        if (blockImg == null || playText == null) return;
        blockImg.color = playText.color = containerColor;
    }

    public void UpdateTitle()
    {
        if (titleText == null) return;
        titleText.text = title;
    }

    public void UpdateImage()
    {
        if (minigameImg == null) return;
        minigameImg.preserveAspect = true;
        minigameImg.sprite = minigameSpr;
    }
}
