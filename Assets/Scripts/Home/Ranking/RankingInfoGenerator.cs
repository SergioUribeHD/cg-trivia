﻿using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class RankingInfoGenerator : MonoBehaviour
{
    [SerializeField] private string usernameListUrl = "http://ec2-18-206-205-69.compute-1.amazonaws.com:8080/uac/users/fillUserList/";

    private List<RankingInfo>  usersInfo = new List<RankingInfo>();

    public static event System.Action<List<RankingInfo>> OnInfoGenerated = null;

    public void GenerateInfoRows(JSONNode rankingNode, JSONNode selectedOptionNode, RankingType type)
    {
        usersInfo.Clear();
        if (type == RankingType.EnrollmentGroupsCategory)
            GenerateEGRows(rankingNode, selectedOptionNode);
        else
            GenerateUserRows(rankingNode);
    }

    private void GenerateEGRows(JSONNode rankingNode, JSONNode selectedOptionNode)
    {
        for (int i = 0; i < rankingNode.Count; i++)
        {
            var rankingInfo = new RankingInfo
            {
                Score = rankingNode[i]["points"],
                Name = SessionData.GetEGNameWithCatTriviaId(selectedOptionNode["triviaId"])
            };
            usersInfo.Add(rankingInfo);
        }
        OnInfoGenerated?.Invoke(usersInfo);
    }

    private void GenerateUserRows(JSONNode rankingNode)
    {
        var usernameIdList = new UsernameIdList(rankingNode.Count) { organizationId = User.OrgId };
        for (int i = 0; i < rankingNode.Count; i++)
        {
            int id = rankingNode[i]["userId"];
            var rankingInfo = new RankingInfo
            {
                Id = id,
                Score = rankingNode[i]["points"]
            };
            usersInfo.Add(rankingInfo);
            usernameIdList.userList[i] = id;
        }
        var restObj = new RestApiObject
        {
            uri = usernameListUrl,
            token = User.Token,
            bodyJson = usernameIdList,
            onSuccess = HandleUsernameRequestSucceeded
        };
        //Debug.Log(JsonUtility.ToJson(restObj.bodyJson));
        RestApiRequest.Instance.PostJson(restObj);
    }

    private void HandleUsernameRequestSucceeded(DownloadHandler response)
    {
        var usersInfoNode = JSON.Parse(response.text);
        for (int i = 0; i < usersInfoNode.Count; i++)
        {
            var user = usersInfo.Find(u => u.Id == usersInfoNode[i]["id"]);
            user.Name = $"{usersInfoNode[i]["firstname"].Value} {usersInfoNode[i]["lastname"].Value}";
        }
        OnInfoGenerated?.Invoke(usersInfo);
    }
}

public class RankingTypeData
{
    public int rankingType;
    public int subRankingType;
    public long categoryId;
    public long egId;
    public long userId;
}

public class RankingInfo
{
    public long Id { get; set; }
    public string Name { get; set; }
    public int Score { get; set; }
}
