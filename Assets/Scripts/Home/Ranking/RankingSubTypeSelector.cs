﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankingSubTypeSelector : MonoBehaviour
{
    [SerializeField] private RankingSubType subType = RankingSubType.Points;
    [SerializeField] private Button button = null;
    [SerializeField] private Image image = null;
    [SerializeField] private Color selectedColor = Color.white;

    private Color originalColor;

    public static event Action<RankingSubType> OnSelect = null;

    private static RankingSubTypeSelector selected;

    private void Awake()
    {
        originalColor = image.color;
        button.onClick.AddListener(Select);
    }

    public void Select()
    {
        OnSelect?.Invoke(subType);
        if (selected != null) selected.Deselect();
        selected = this;
        image.color = selectedColor;
        button.interactable = false;
    }

    public void Deselect()
    {
        image.color = originalColor;
        button.interactable = true;
    }
}
