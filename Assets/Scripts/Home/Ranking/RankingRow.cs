﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankingRow : MonoBehaviour
{
    [SerializeField] private Image positionImg = null;
    [SerializeField] private Text positionText = null;
    [SerializeField] private Text nameText = null;
    [SerializeField] private Text scoreText = null;

    public void Set(RankingInfo data, int position)
    {
        gameObject.SetActive(true);
        positionImg.enabled = position < 4;
        positionText.text = position < 4 ? string.Empty : position.ToString();
        nameText.text = data.Name;
        scoreText.text = data.Score.ToString();
    }
}

public class RankingRowData
{
    public long UserId { get; private set; }
    public string Username { get; private set; }
    public int Score { get; private set; }

    public RankingRowData(long userId, string username, int score)
    {
        UserId = userId;
        Username = username;
        Score = score;
    }
}
