using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class RankingTypeSelector : MonoBehaviour
{
    [SerializeField] private Dropdown typesDdwn = null;
    [SerializeField] private Dropdown optionsDdwn = null;
    [SerializeField] private UserEgSwitcher userEgSwitcher = null;
    [SerializeField] private RankingLoader loader = null;
    [SerializeField] private RankingSubTypeSelector defaultSubType = null;

    JSONNode SelectedOption => optionsNode.Count == 0 ? null : optionsNode[optionIndex];

    private RankingType type;
    private RankingSubType subType;
    private List<JSONNode> optionsNode = new List<JSONNode>();
    private int optionIndex;

    private void Awake()
    {
        typesDdwn.onValueChanged.AddListener(HandleRankingTypeSelected);
        optionsDdwn.onValueChanged.AddListener(HandleOptionSelected);
        userEgSwitcher.Switcher.OnSwitch.AddListener(HandleSwitch);
        RankingSubTypeSelector.OnSelect += HandleSubtypeSelected;
    }

    private void Start()
    {
        HandleRankingTypeSelected(0);
    }

    private void OnDestroy()
    {
        RankingSubTypeSelector.OnSelect -= HandleSubtypeSelected;
    }

    private void HandleRankingTypeSelected(int index)
    {
        optionIndex = 0;
        type = (RankingType)(index + 1);
        userEgSwitcher.gameObject.SetActive(type == RankingType.UserCategory);
        optionsDdwn.gameObject.SetActive(type != RankingType.User);
        optionsNode.Clear();
        if (type == RankingType.UserCategory)
        {
            optionsNode.AddRange(SessionData.CategoriesData);
            if (userEgSwitcher.Switcher.IsOn) type = RankingType.EnrollmentGroupsCategory;
        }
        else if (type == RankingType.EnrollmentGroup)
            optionsNode.AddRange(SessionData.EnrollmentGroupsData);
        SetCategoryDropdownOptions();
        defaultSubType.Select();
    }

    private void HandleOptionSelected(int index)
    {
        optionIndex = index;
        loader.LoadRanking(SelectedOption, type, subType);
    }

    private void HandleSwitch(bool isOn)
    {
        type = isOn ? RankingType.EnrollmentGroupsCategory : RankingType.UserCategory;
        loader.LoadRanking(SelectedOption, type, subType);
    }

    private void HandleSubtypeSelected(RankingSubType subType)
    {
        this.subType = subType;
        loader.LoadRanking(SelectedOption, type, subType);
    }

    private void SetCategoryDropdownOptions()
    {
        optionsDdwn.ClearOptions();
        var options = new List<string>();
        foreach (var opt in optionsNode)
        {
            options.Add(opt["name"]);
        }
        optionsDdwn.AddOptions(options);
    }

    
}
