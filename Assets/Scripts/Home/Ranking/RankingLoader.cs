using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class RankingLoader : MonoBehaviour
{
    [SerializeField] private string rankingUrl = "http://ec2-18-206-205-69.compute-1.amazonaws.com:8080/cgd/game/trivia/getRanking/";
    [SerializeField] private RankingInfoGenerator generator = null;

    private RankingType type;
    private JSONNode selectedOption;

    public void LoadRanking(JSONNode selectedOption, RankingType type, RankingSubType subType)
    {
        this.type = type;
        this.selectedOption = selectedOption;
        RestApiObject restObj = new RestApiObject
        {
            uri = rankingUrl,
            bodyJson = GetRankingTypeData(selectedOption, type, subType),
            token = User.Token,
            onSuccess = HandleRankingLoaded
        };
        //Debug.Log(JsonUtility.ToJson(restObj.bodyJson));
        //Debug.Log(JsonUtility.ToJson(restObj));
        RestApiRequest.Instance.PostJson(restObj);
    }

    private RankingTypeData GetRankingTypeData(JSONNode selectedOption, RankingType type, RankingSubType subType)
    {
        var data = new RankingTypeData
        {
            rankingType = (int)type,
            subRankingType = (int)subType
        };
        switch (type)
        {
            case RankingType.UserCategory:
            case RankingType.EnrollmentGroupsCategory:
                data.categoryId = selectedOption["id"];
                break;
            case RankingType.EnrollmentGroup:
                data.egId = selectedOption["id"];
                break;
            case RankingType.User:
                data.userId = User.Id;
                break;
        }
        return data;
    }

    private void HandleRankingLoaded(DownloadHandler response)
    {
        generator.GenerateInfoRows(JSON.Parse(response.text), selectedOption, type);
    }
}
