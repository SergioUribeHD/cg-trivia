﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserEgSwitcher : MonoBehaviour
{
    [field: SerializeField] public Switcher Switcher { get; private set; }
    [SerializeField] private Text userEgText = null;
    [SerializeField] private string userTitle = string.Empty;
    [SerializeField] private string egTitle = string.Empty;

    private void Awake()
    {
        Switcher.OnSwitch.AddListener(HandleSwitch);
    }

    private void HandleSwitch(bool on)
    {
        userEgText.text = on ? egTitle : userTitle;
    }
}
