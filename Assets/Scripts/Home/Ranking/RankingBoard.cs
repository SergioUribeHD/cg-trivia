﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RankingBoard : MonoBehaviour
{
    [SerializeField] private List<RankingRow> rankingRows = null;
    [SerializeField] private RectTransform content = null;
    [SerializeField] private UnityEvent onBoardSet = null;

    private void Awake()
    {
        RankingInfoGenerator.OnInfoGenerated += Set;
    }
    private void OnDisable() 
    {
        for (int i = 0; i < content.transform.childCount; i++)
            Destroy(content.GetChild(i).gameObject);
    }

    private void OnDestroy()
    {
        RankingInfoGenerator.OnInfoGenerated -= Set;
    }

    private void Set(List<RankingInfo> infoRows)
    {
        if (rankingRows.Count == 0)
        {
            Debug.LogWarning($"You need to have some instance references in {name} for method {nameof(Set)} to work!");
            return;
        }
        CheckMoreInstancesNeeded(infoRows.Count);
        for (int i = 0; i < infoRows.Count; i++)
            rankingRows[i].Set(infoRows[i], i + 1);
        for (int i = infoRows.Count; i < rankingRows.Count; i++)
            rankingRows[i].gameObject.SetActive(false);
        onBoardSet?.Invoke();
    }

    private void CheckMoreInstancesNeeded(int infoRowsCount)
    {
        int diff = infoRowsCount - rankingRows.Count;
        if (diff <= 0) return;
        var rowModel = rankingRows.GetLastItem();
        for (int i = 0; i < diff; i++)
            rankingRows.Add(Instantiate(rowModel, content));
    }

}
