using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DropdownOption
{
    Category,
    EnrollmentGroup,
    None
}
