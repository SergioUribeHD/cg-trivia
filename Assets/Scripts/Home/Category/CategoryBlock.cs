﻿using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.UI;

public class CategoryBlock : MonoBehaviour
{
    [SerializeField] private Text categoryTitleText = null;
    [SerializeField] private Image image = null;

    /// <summary>
    /// Used to retrieve all the questions that belong to the category
    /// </summary>
    private JSONNode questionModules;

    /// <summary>
    /// Basic attributes of the category
    /// </summary>
    private CategoryData data;

    public long EnrollmentGroupId { get; private set; }
    public void Set(JSONNode node, Color color)
    {
        image.color = color;
        if (node == null)
        {
            categoryTitleText.text = "Test";
            return;
        }
        data = JsonUtility.FromJson<CategoryData>(node.ToString());
        categoryTitleText.text = data.name;
        questionModules = SessionData.GetCatQuestionModules(node);
        EnrollmentGroupId = SessionData.GetEGId(data.triviaId);
    }
     
    public void Select()
    {
        if (questionModules == null) return;
        CategoryLoader.Instance.Load(data, questionModules);   
    }

}


