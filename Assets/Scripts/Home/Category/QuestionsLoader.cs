using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class QuestionsLoader : MonoBehaviour
{
    [SerializeField]
    private string[] modulesUrls = new string[]
    {
        "http://ec2-18-206-205-69.compute-1.amazonaws.com:8080/games/trivia/getTriviaQuestionByListID/",
        "http://ec2-18-206-205-69.compute-1.amazonaws.com:8080/quiz-game/quiz/getTriviaQuestionByListID/",
        "http://ec2-18-206-205-69.compute-1.amazonaws.com:8080/cert-game/certification/getTriviaQuestionByListID/"
    };

    public event System.Action<List<QuestionData>> OnLoadFinished = null;

    private List<QuestionData> questions = new List<QuestionData>();

    private int loadedModuleCount;


    public void Load(JSONNode questionModules)
    {
        var restApiObject = new RestApiObject
        {
            token = User.Token,
            onSuccess = HandleCategoryQuestionsLoaded
        };

        for (int i = 0; i < modulesUrls.Length; i++)
        {
            restApiObject.uri = modulesUrls[i];
            restApiObject.bodyJson = new QuestionIdList(i, questionModules);
            RestApiRequest.Instance.PostJson(restApiObject);
        }
    }

    private void HandleCategoryQuestionsLoaded(DownloadHandler response)
    {
        var questionsData = JSON.Parse(response.text);
        foreach (var question in questionsData)
        {
            var questionData = JsonUtility.FromJson<QuestionData>(question.Value.ToString());
            questions.Add(questionData);
        }
        loadedModuleCount++;
        CheckAllModulesLoaded();
    }

    private void CheckAllModulesLoaded()
    {
        if (loadedModuleCount < modulesUrls.Length) return;
        OnLoadFinished?.Invoke(questions);
    }

}
