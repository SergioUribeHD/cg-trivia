using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectorHorizontalMover
{
    HorizontalLayoutGroup contentLayout;
    RectTransform contentRect;
    RectTransform prefab;

    private Vector2 contentOriginalPos;
    private Vector2 contentLimitPos;

    private Vector2 NewPos => new Vector2(contentLayout.spacing + prefab.rect.width, 0);
    public bool LeftLimitReached => Vector2.Distance(contentLimitPos, contentRect.anchoredPosition) < 0.1f;
    public bool RightLimitReached => Vector2.Distance(contentOriginalPos, contentRect.anchoredPosition) < 0.1f;


    public SelectorHorizontalMover(HorizontalLayoutGroup content, Transform prefab, int instancesAmount)
    {
        contentLayout = content;
        contentRect = (RectTransform)(content.transform);
        this.prefab = (RectTransform)prefab;
        contentOriginalPos = contentRect.anchoredPosition;
        contentLimitPos = contentOriginalPos - new Vector2((this.prefab.rect.width + contentLayout.spacing) * (instancesAmount - 1), 0);
    }

    public void Reset() => contentRect.anchoredPosition = contentOriginalPos;

    public void MoveRight() => contentRect.anchoredPosition += NewPos;

    public void MoveLeft() => contentRect.anchoredPosition -= NewPos;
}
