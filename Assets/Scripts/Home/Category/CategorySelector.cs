using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CategorySelector : MonoBehaviour
{
    [SerializeField] private List<CategoryBlock> categoryBlocks = null;
    [SerializeField] private List<Color> blockColors = new List<Color>() { Color.white };
    [SerializeField] private HorizontalLayoutGroup content = null;
    [SerializeField] private GameObject nextBtn = null;
    [SerializeField] private GameObject prevBtn = null;

    [field: SerializeField] public UnityEvent OnCategoryReady { get; private set; }

    private SelectorHorizontalMover mover;

    private void Awake()
    {
        mover = new SelectorHorizontalMover(content, categoryBlocks.GetLastItem().transform, SessionData.CategoriesData.Count);
        CategoryLoader.OnLoadFinished += HandleCategoryReady;
    }

    //private void OnEnable()
    //{
    //    mover.Reset();
    //    CheckLimits();
    //}

    private void Start()
    {
        int dataCount = SessionData.CategoriesData.Count;
        var colors = blockColors.GetListOfRandomItems(dataCount);
        CheckBlockInstancesCount();
        if (dataCount == 0)
        {
            Debug.LogWarning("User not logged in.");
            categoryBlocks[0].Set(null, blockColors[0]);
        }
        else
            SetCategoryBlocks(dataCount, colors);
    }

    private void SetCategoryBlocks(int dataCount, List<Color> colors)
    {
        for (int i = 0; i < dataCount; i++)
        {
            categoryBlocks[i].gameObject.SetActive(true);
            categoryBlocks[i].Set(SessionData.CategoriesData[i], colors.RemoveLastItem());
        }
        for (int i = dataCount; i < categoryBlocks.Count; i++)
            categoryBlocks[i].gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        CategoryLoader.OnLoadFinished -= HandleCategoryReady;
    }

    private void CheckBlockInstancesCount()
    {
        int diff = SessionData.CategoriesData.Count - categoryBlocks.Count;
        if (diff > 0)
        {
            var blockModel = categoryBlocks.GetLastItem();
            for (int i = 0; i < diff; i++)
                categoryBlocks.Add(Instantiate(blockModel, content.transform));
        }
    }

    private void HandleCategoryReady(Category category)
    {
        SessionData.CurrentCategory = category;
        OnCategoryReady?.Invoke();
    }

    public void MoveNext()
    {
        mover.MoveLeft();
        CheckLimits();
    }

    public void MoveBack()
    {
        mover.MoveRight();
        CheckLimits();
    }

    private void CheckLimits()
    {
        nextBtn.SetActive(!mover.LeftLimitReached);
        prevBtn.SetActive(!mover.RightLimitReached);
    }
}
