using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CategoryLoader : Singleton<CategoryLoader>
{
    [SerializeField] private QuestionsLoader questionsLoader = null;
    [SerializeField] private ProgressLoader progressLoader = null;

    public static event System.Action<Category> OnLoadFinished = null;

    private Category loadedCategory;

    private bool progressLoadFinished, questionsLoadFinished;

    private void Awake()
    {
        questionsLoader.OnLoadFinished += HandleQuestionsLoadFinished;
        progressLoader.OnLoadFinished += HandleProgressLoadFinished;
    }

    private void OnDestroy()
    {
        questionsLoader.OnLoadFinished -= HandleQuestionsLoadFinished;
        progressLoader.OnLoadFinished -= HandleProgressLoadFinished;
    }

    public void Load(CategoryData data, JSONNode questionModules)
    {
        if (SessionData.Categories.TryFind(c => c.Equals(data), out var cat))
        {
            OnLoadFinished?.Invoke(cat);
            return;
        }
        loadedCategory = new Category(data) { Id = data.id, EnrollmentGroupId = SessionData.GetEGId(data.triviaId) };
        questionsLoader.Load(questionModules);
        progressLoader.Load(data);
    }

    private void HandleProgressLoadFinished(CategoryStats stats)
    {
        loadedCategory.LoadStats(stats);
        progressLoadFinished = true;
        CheckLoadFinished();
    }

    private void HandleQuestionsLoadFinished(List<QuestionData> questions)
    {
        loadedCategory.QuestionsData.AddRange(questions);
        questionsLoadFinished = true;
        CheckLoadFinished();
    }

    private void CheckLoadFinished()
    {
        if (!questionsLoadFinished || !progressLoadFinished) return;
        SessionData.Categories.Add(loadedCategory);
        OnLoadFinished?.Invoke(loadedCategory);
    }
}
