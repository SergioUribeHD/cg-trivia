using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ProgressLoader : MonoBehaviour
{
    [SerializeField] private VarReplacer url = new VarReplacer("http://ec2-18-206-205-69.compute-1.amazonaws.com:8080/cgd/game/trivia/findCategoryAllByUser/{categoryId}/{egId}/{userId}");

    public event System.Action<CategoryStats> OnLoadFinished = null;

    public void Load(CategoryData data)
    {
        var uri = url.GetReplacedText(new object[] { data.id, SessionData.GetEGId(data.triviaId), User.Id } );
        Debug.Log(uri);
        var restObj = new RestApiObject
        {
            uri = uri,
            token = User.Token,
            onSuccess = HandleCompletedStagesLoaded
        };
        RestApiRequest.Instance.GetJson(restObj);

    }

    private void HandleCompletedStagesLoaded(DownloadHandler response)
    {
        OnLoadFinished?.Invoke(JsonUtility.FromJson<CategoryStats>(response.text));
    }
}
