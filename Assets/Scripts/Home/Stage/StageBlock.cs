﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageBlock : MonoBehaviour
{
    [SerializeField] private Button button = null;
    [SerializeField] private Text stageText = null;
    [SerializeField] private GameObject blockedMark = null;
    [SerializeField] private Text infoText = null;
    [SerializeField] private VarReplacer infoVarReplacer = null;

    public static event Action<StageBlock> OnStageLoaded = null;

    public void LoadStage()
    {
        OnStageLoaded?.Invoke(this);
    }

    public void Enable(string stageName, StageInfo stageInfo, bool locked)
    {
        gameObject.SetActive(true);
        stageText.text = stageName;
        button.interactable = !locked;
        string score = locked ? string.Empty : stageInfo.Score.ToString();
        string attempts = locked ? string.Empty : stageInfo.Attempts.ToString();
        infoText.text = infoVarReplacer.GetReplacedText(new string[] { score, attempts });
        blockedMark.SetActive(locked);
    }

    private void OnDisable()
    {
        stageText.text = string.Empty;
    }
}
