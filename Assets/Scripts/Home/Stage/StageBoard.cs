﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class StageBoard : MonoBehaviour
{
    [SerializeField] private Text progressText = null;
    [SerializeField] private VarReplacer progressVarReplacer = null;
    [SerializeField] private List<StageBlock> stageBlocks = new List<StageBlock>();
    [SerializeField] private RectTransform content = null;
    [SerializeField] private string stageName = "Etapa";
    [SerializeField] private int defaultStagesAmount = 10;
    [SerializeField] private UnityEvent onStageLoaded = null;

    private int stagesAmount;
    private int currentStage;

    private void Awake()
    {
        StageBlock.OnStageLoaded += HandleStageLoaded;
    }

    private void OnEnable()
    {
        SetInitialValues();
        CheckEnoughBlocks();
        StartCoroutine(CallBlocksEnable());
    }

    private IEnumerator CallBlocksEnable()
    {
        yield return new WaitForEndOfFrame();
        int[] catProgressValues = new int[]
        {
            SessionData.CurrentCategory.Score,
            SessionData.CurrentCategory.CompletedStagesCount,
            SessionData.CurrentCategory.Data.stages
        };
        progressText.text = progressVarReplacer.GetReplacedText(catProgressValues);
        EnableBlocks();
    }

    private void SetInitialValues()
    {
        if (SessionData.CurrentCategory != null)
            SessionData.CurrentCategory.CurrentStage = null;
        stagesAmount = SessionData.CurrentCategory == null ? defaultStagesAmount : SessionData.CurrentCategory.Data.stages;
        currentStage = SessionData.CurrentCategory == null ? 0 : SessionData.CurrentCategory.CompletedStagesCount;
    }

    private void OnDestroy()
    {
        StageBlock.OnStageLoaded -= HandleStageLoaded;
    }

    private void CheckEnoughBlocks()
    {
        int diff = stagesAmount - stageBlocks.Count;
        if (diff > 0)
        {
            Debug.LogWarning($"Not enough blocks in {name}. Adding more.");
            for (int i = 0; i < diff; i++)
                Instantiate(stageBlocks[0], content);
        }
    }

    private void EnableBlocks()
    {
        for (int i = 0; i < stagesAmount; i++)
        {
            string stageName = $"{this.stageName} {i + 1}";
            bool locked = currentStage < i;
            var stage = i < SessionData.CurrentCategory.CompletedStagesCount ? SessionData.CurrentCategory.Stages[i] : new StageInfo();
            stageBlocks[i].Enable(stageName, stage, locked);
        }

        for (int i = stagesAmount; i < stageBlocks.Count; i++)
        {
            if (stageBlocks[i].gameObject.activeSelf)
                stageBlocks[i].gameObject.SetActive(false);
        }
    }

    private void HandleStageLoaded(StageBlock block)
    {
        if (SessionData.CurrentCategory != null)
        {
            int index = stageBlocks.IndexOf(block);
            if (index == SessionData.CurrentCategory.Stages.Count)
                SessionData.CurrentCategory.Stages.Add(new StageInfo());
            SessionData.CurrentCategory.CurrentStage = SessionData.CurrentCategory.Stages[index];
        }
        onStageLoaded?.Invoke();
    }
}


